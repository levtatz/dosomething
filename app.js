const express = require('express')
const app = express()

const queue = require('./queue')

const bodyParser = require('body-parser');
const multer = require('multer'); // v1.0.5
const upload = multer(); // for parsing multipart/form-data

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.post('/send', upload.array(), function (req, res) {
  var response = queue.send(req.body);
  res.json(response)
});

app.use('/', express.static('public'))

app.listen(process.env.PORT || 3000, () => console.log('Listening on port 3000!'))
